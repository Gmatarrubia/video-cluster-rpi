#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: videoWall.py                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
from Tkinter import *

class Infovw(Frame):
    def __init__(self, parent, ventana, gestor):
        self.dicServidores = gestor.dicServidores
        pageVW = ventana.pageVideoWalls
        self.gestorvw = parent
        Frame.__init__(self, pageVW)
        self.etvar = {}
        self.titulo = ventana.crearEtiqueta(self, "Parametros", 0, 0, 1, 3)
        self.titulo.config(font=("Courier", 16))
        #Arbol
        self.arbol = ventana.crearLabelFrame(self, "Arbol del VideoWall:", 1, 0)
        #self.img = ventana.crearImginfo(self.arbol, "rpipeq.png", 0, 0, "hola")
        #Valores actuales
        self.info = ventana.crearLabelFrame(self, "Valores Actuales:", 2, 0)
        self.etx = ventana.crearEtiqueta(self.info, "Coords[cm] x:", 0, 0)
        self.ety = ventana.crearEtiqueta(self.info, "Coords[cm] y:", 1, 0)
        self.etfe = ventana.crearEtiqueta(self.info, "Factor escala:", 2, 0)
        self.etrot = ventana.crearEtiqueta(self.info, "Angulo rot:", 3, 0)
        self.etvar['etx1var'] = ventana.crearEtiqueta(self.info, "0", 0, 1)
        self.etvar['ety1var'] = ventana.crearEtiqueta(self.info, "0", 1, 1)
        self.etvar['etx2var'] = ventana.crearEtiqueta(self.info, "0", 0, 2)
        self.etvar['ety2var'] = ventana.crearEtiqueta(self.info, "0", 1, 2)
        self.etvar['etfevar'] = ventana.crearEtiqueta(self.info, "0", 2, 1)
        self.etvar['etrotvar'] = ventana.crearEtiqueta(self.info, "0", 3, 1)
        for etiqueta in self.etvar:
            self.etvar[etiqueta].config(font=("Times", 10, "bold"))
        #Cambiar valores manualmente
        self.manual = ventana.crearLabelFrame(self, "Valores manuales:", 3, 0)
        self.config(width=300, relief=GROOVE, borderwidth=2)
        self.valText = {}
        self.val = {}
        oLado = self.opcionLado = IntVar(self.manual, 1)
        self.valText['text'] = ventana.crearEtiqueta(self.manual, \
                                                     "Coordenandas Esquina Sup. Izq.", 0, 0, 1, 2)
        self.valText['x'] = ventana.crearEtiqueta(self.manual, "X[cm]: ", 1, 0)
        self.val['x'] = ventana.crearEntrada(self.manual, 1, 1)
        self.valText['y'] = ventana.crearEtiqueta(self.manual, "Y[cm]: ", 2, 0)
        self.val['y'] = ventana.crearEntrada(self.manual, 2, 1)
        self.valText['lado'] = ventana.crearEtiqueta(self.manual, "Lado[cm]:", 3, 0)
        self.val['lado'] = ventana.crearEntrada(self.manual, 4, 0)
        self.ladoh = ventana.crearRadioBot(self.manual, "Horizontal", oLado, 1, 3, 1)
        self.ladov = ventana.crearRadioBot(self.manual, "Vertical", oLado, 0, 4, 1)
        self.valText['rot'] = ventana.crearEtiqueta(self.manual, "Rotacion[º]: ", 5, 0)
        self.bot = ventana.crearBoton(self.manual, "Aplicar", self.aplicarValores, NORMAL, 6, 0)
        self.bot.grid(row=6, column=0, rowspan=1, columnspan=2)
        self.val['rot'] = ventana.crearEntrada(self.manual, 5, 1)
        for entry in self.val:
            self.val[entry].config(width=8)

    def aplicarValores(self):
        canvasXcm = self.gestorvw.MedidasVWcm[0]
        canvasYcm = self.gestorvw.MedidasVWcm[1]
        canvasX = self.gestorvw.canvasW
        canvasY = self.gestorvw.canvasH
        x1cm = float(self.val['x'].get())
        y1cm = float(self.val['y'].get())
        opcion = self.opcionLado.get()
        ladoh = 0
        ladov = 0
        proporcional = 0
        rectangulos = self.gestorvw.screen.find_withtag('rec')
        seleccion = None
        for item in rectangulos:
            if self.gestorvw.screen.itemcget(item, 'fill') == 'red':
                seleccion = item
                break
        tags = self.gestorvw.screen.gettags(seleccion)
        if tags[2] == '16:9':
            proporcional = 1.777
        elif tags[2] == '4:3':
            proporcional = 1.333
        elif tags[2] == '1:1':
            proporcional = 1
        if opcion:
            ladoh = float(self.val['lado'].get())
            ladov = round(ladoh/proporcional, 2)
        else:
            ladov = float(self.val['lado'].get())
            ladoh = round(ladov*proporcional, 2)
        x2cm = x1cm + ladoh
        y2cm = y1cm + ladov
        coords = []
        coords.append(round((x1cm*canvasX)/canvasXcm, 2))
        coords.append(round((y1cm*canvasY)/canvasYcm, 2))
        coords.append(round((x2cm*canvasX)/canvasXcm, 2))
        coords.append(round((y1cm*canvasY)/canvasYcm, 2))
        coords.append(round((x2cm*canvasX)/canvasXcm, 2))
        coords.append(round((y2cm*canvasY)/canvasYcm, 2))
        coords.append(round((x1cm*canvasX)/canvasXcm, 2))
        coords.append(round((y2cm*canvasY)/canvasYcm, 2))
        self.gestorvw.fijarposicion(coords)



    def actualizarCoords(self, valores):
        self.etvar['etx1var'].config(text=str(valores[0]))
        self.etvar['ety1var'].config(text=str(valores[2]))
        self.etvar['etx2var'].config(text=str(valores[1]))
        self.etvar['ety2var'].config(text=str(valores[3]))

    def actualizarRot(self, rot):
        self.etvar['etrotvar'].config(text=str(rot))

    def actualizarEscala(self, escala):
        self.etvar['etfevar'].config(text=str(escala))
