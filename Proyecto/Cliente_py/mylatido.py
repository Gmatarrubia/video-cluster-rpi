#!/usr/bin/env python
# -*- coding: cp1252 -*-.

"""*********************************************************************
 * Archivo: funciones.py                                               *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 ********************************************************************"""
import time
from threading import Thread
import zmq
from mensaje_pb2 import Latido

class mylatido(Thread):
    def __init__(self, dsocket, gestor):
        Thread.__init__(self, target=self.recibirIP, args=(dsocket, gestor))
    def recibirIP(self, dSocket, gestor):
        poller = zmq.Poller()
        poller.register(dSocket.socket, zmq.POLLIN)
        paquete = dict(poller.poll())
        while True:
            if paquete.get(dSocket.socket) == zmq.POLLIN:
                data = dSocket.socket.recv()
                estado = Latido()
                estado.ParseFromString(data)
                if estado.IP != "":
                    if gestor.servidorIncluido(estado.IP):
                        #Registra el tiempo del latido
                        gestor.LatidoServidor(estado, time.time())
                    else:
                        #Apuntar el tiempo de latido
                        gestor.addServidor(estado)
            time.sleep(0.1)
