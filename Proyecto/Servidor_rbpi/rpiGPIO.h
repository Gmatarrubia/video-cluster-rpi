/***********************************************************************
 * Archivo: rpiGPIO.h                                                  *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo    *
 * Matarrubia.                                                         *
 *  ETSIDI - Universidad Politecnica de Madrid                         *
 *  Licencia de uso, distribucion y modificacion:                      *
 *  GNU General Public License (GPL) 3.0                               *
 **********************************************************************/
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>

#define IN   0
#define OUT  1

#define LOW  0
#define HIGH 1

#ifndef SERVIDOR_RBPI_RPIGPIO_H
#define SERVIDOR_RBPI_RPIGPIO_H

string GPIO_EXPORT = "/sys/class/gpio/export";
string GPIO_UNEXPORT = "/sys/class/gpio/unexport";
int PIN[] = {17, 18, 27, 22, 23, 24, 25};
using namespace std;

void GPIOExport(int pin){
#define BUFFER_MAX 3
    char buffer[BUFFER_MAX];
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (-1 == fd) {
        fprintf(stderr, "Failed to open export for writing!\n");
        return;
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
    write(fd, buffer, bytes_written);
    close(fd);
    return;
}

int GPIOUnexport(int pin){
    char buffer[BUFFER_MAX];
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (-1 == fd) {
        fprintf(stderr, "Failed to open unexport for writing!\n");
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
    write(fd, buffer, bytes_written);
    close(fd);
    return(0);
}

int GPIODirection(int pin, int dir){
    static const char s_directions_str[]  = "in\0out";
#define DIRECTION_MAX 35
    char path[DIRECTION_MAX];
    int fd;

    snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd) {
        fprintf(stderr, "Failed to open gpio direction for writing!\n");
        return(-1);
    }

    if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
        fprintf(stderr, "Failed to set direction!\n");
        return(-1);
    }

    close(fd);
    return(0);
}

int GPIORead(int pin) {
#define VALUE_MAX 30
    char path[VALUE_MAX];
    char value_str[3];
    int fd;

    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
    fd = open(path, O_RDONLY);
    if (-1 == fd) {
        fprintf(stderr, "Failed to open gpio value for reading!\n");
        return(-1);
    }

    if (-1 == read(fd, value_str, 3)) {
        fprintf(stderr, "Failed to read value!\n");
        return(-1);
    }

    close(fd);

    return(atoi(value_str));
}

int GPIOWrite(int pin, int value){
    static const char s_values_str[] = "01";

    char path[VALUE_MAX];
    int fd;

    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd) {
        fprintf(stderr, "Failed to open gpio value for writing!\n");
        return(-1);
    }

    if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
        fprintf(stderr, "Failed to write value!\n");
        return(-1);
    }

    close(fd);
    return(0);
}

string getIPrpiGPIO() {
    cout<<"Configuracion IP por pines RPI"<<endl;
    int valores[] = {0, 0, 0, 0, 0, 0, 0};
    int intIP = 0, pos = 0, ncluster = 0;
    //Inicializar los puertos
    for (int i=0; i<7; i++){
        GPIOExport(PIN[i]);
    }
    //Configurar como pines de entrada
    for (int i=0; i<7; i++){
        GPIODirection(PIN[i], IN);
    }
    //Leer valores de los pines
    for (int i=0; i<7; i++){
       valores[i]  = GPIORead(PIN[i]);
        cout << valores[i] << endl;
    }
    //Cerramos los pines
    for (int i=0; i<7; i++){
        GPIOUnexport(PIN[i]);
    }
    //Obteniendo posicion dentro del cluster
    if (valores[0]) pos |= 1 << 2;
    if (valores[1]) pos |= 1 << 1;
    if (valores[2]) pos |= 1;
    //Obteniendo numero del cluster
    if (valores[3]) ncluster |= 1 << 3;
    if (valores[4]) ncluster |= 1 << 2;
    if (valores[5]) ncluster |= 1 << 1;
    if (valores[6]) ncluster |= 1;
    intIP = (ncluster*10)+pos;
    std::string IP;
    stringstream buffer(IP);
    buffer << intIP;
    IP = "192.168.1." + buffer.str();
    cout<<"IP fijada: " << IP << endl;
    return IP;
}


#endif //SERVIDOR_RBPI_RPIGPIO_H
