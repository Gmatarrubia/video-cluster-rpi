/***********************************************************************
 * Archivo: main.cpp                                                   *
 * Este es un documento que forma parte del Trabajo Fin de Grado de    *
 * Coordinacion de video con cluster raspberry pi hecho por Gonzalo     *
 * Matarrubia.                                                         *
 **********************************************************************/
#include <zmq.hpp>
#include <iostream>
#include "mensaje.pb.h"

#ifndef SERVIDOR_RBPI_CONN_ZMQ_H
#define SERVIDOR_RBPI_CONN_ZMQ_H

using namespace zmq;

/* Crea un socket que es capad de envíar datos de tipo
 * State y Command serializados con protobuffers    */
class conn_zmq {
protected:
    socket_t s;
public:
    //El contexto es el mismo para todos los sockets
    static context_t context;
    Command msjCmd;
    State msjSt;
    bool connected = false;
public:
    conn_zmq(int type): s(context, type) {}
    void bind(std::string dirIP);
    void connect(std::string dirIP);
    Command recv_command( );
    void send_command(Command msjCmd);
    void send_state( State msjSt );
};

#endif //SERVIDOR_RBPI_CONN_ZMQ_H
